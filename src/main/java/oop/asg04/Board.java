// Board.java
package oop.asg04;
/**
 CS108 Tetris Board.
 Represents a Tetris board -- essentially a 2-d grid
 of booleans. Supports tetris pieces and row clearing.
 Has an "undo" feature that allows clients to add and remove pieces efficiently.
 Does not do any drawing or have any idea of pixels. Instead,
 just represents the abstract 2-d board.
*/
public class Board	{
	// Some ivars are stubbed out for you:
	private int width;
	private int height;
	private boolean[][] grid;
	private boolean DEBUG = true;
	boolean committed;

	private int[] widths;
	private int[] heights;
	private int maxHeight; // gia tri lon nhat cua heights;
	private boolean[][] gridBackUp;
	private int[] widthsBackUp;

	// Here a few trivial methods are provided:
	
	/**
	 Creates an empty board of the given width and height
	 measured in blocks.
	*/
	public Board(int width, int height) {
		this.width = width;
		this.height = height;
		grid = new boolean[width][height];
		committed = true;
		widths = new int[height]; // so cac o da lap tai moi hang
		heights = new int[width]; // chieu cao ma moi cot da duoc lap
		maxHeight = 0;
		gridBackUp = new boolean[width][height];
		widthsBackUp = new int[height];
		// YOUR CODE HERE
	}
	
	
	/**
	 Returns the width of the board in blocks.
	*/
	public int getWidth() {
		return width;
	}
	
	
	/**
	 Returns the height of the board in blocks.
	*/
	public int getHeight() {
		return height;
	}
	
	
	/**
	 Returns the max column height present in the board.
	 For an empty board this is 0.
	*/
	public int getMaxHeight() {	 
		return maxHeight;
	}
	
	
	/**
	 Checks the board for internal consistency -- used
	 for debugging.
	*/
	public void sanityCheck() {
		if (DEBUG) {

			for (int i = 0; i < width; i++)
				for (int j = height - 1; j >= 0; j--)
					if (grid[i][j] == true)
					{
						if( j + 1 != heights[i] ) {
							throw new RuntimeException("Heights is not updated!");
						}
						break;
					}

			int cnt = 0;
			for( int j = 0 ; j < height ; j++ )
			{
				cnt = 0;
				for ( int i = 0 ; i < width ; i++ )
					if( grid[i][j] == true )
						cnt++;
				if( cnt != widths[j] )
				{
					throw new RuntimeException("Widths is not update!");
				}
			}
			// YOUR CODE HERE
		}
	}

	/**
	 Given a piece and an x, returns the y
	 value where the piece would come to rest
	 if it were dropped straight down at that x.

	 <p>
	 Implementation: use the skirt and the col heights
	 to compute this fast -- O(skirt length).
	 */
	public int dropHeight(Piece piece, int x) {

		int y = 0;
		int[] pieceSkirt = piece.getSkirt();
		for( int i = 0 ; i < pieceSkirt.length ; i++ )
		{
			y = Math.max( heights[ i + x ] - pieceSkirt[i] , y );
		}
		return y; // YOUR CODE HERE
	}
	

	/**
	 Returns the height of the given column --
	 i.e. the y value of the highest block + 1.
	 The height is 0 if the column contains no blocks.
	 */
	public int getColumnHeight(int x) {
		return heights[x];
	}
	
	
	/**
	 Returns the number of filled blocks in
	 the given row.
	*/
	public int getRowWidth(int y) {
		return widths[y];
	}
	
	
	/**
	 Returns true if the given block is filled in the board.
	 Blocks outside of the valid width/height area
	 always return true.
	*/
	public boolean getGrid(int x, int y) {
		if( x > width || y > height || x < 0 || y < 0 )	return true;
		return grid[x][y];
	}
	
	
	public static final int PLACE_OK = 0;
	public static final int PLACE_ROW_FILLED = 1;
	public static final int PLACE_OUT_BOUNDS = 2;
	public static final int PLACE_BAD = 3;
	
	/**
	 Attempts to add the body of a piece to the board.
	 Copies the piece blocks into the board grid.
	 Returns PLACE_OK for a regular placement, or PLACE_ROW_FILLED
	 for a regular placement that causes at least one row to be filled.
	 
	 <p>Error cases:
	 A placement may fail in two ways. First, if part of the piece may falls out
	 of bounds of the board, PLACE_OUT_BOUNDS is returned.
	 Or the placement may collide with existing blocks in the grid
	 in which case PLACE_BAD is returned.
	 In both error cases, the board may be left in an invalid
	 state. The client can use undo(), to recover the valid, pre-place state.
	*/
	private boolean outOfBound( int x , int y )
	{
		return ( x < 0 || y < 0 || x > width - 1 || y > height - 1 );
	}

	private void updateMaxHeight( int x )
	{
		maxHeight = Math.max( maxHeight , heights[x] );
	}
	private void updateGrid( int x , int y )
	{
		if( !outOfBound(x , y) )
			grid[x][y] = true;
	}
	private void updateHeights( int x , int h )
	{
		heights[x] = Math.max( heights[x] , h + 1 );
	}
	public int place(Piece piece, int x, int y) {
		// flag !committed problem
		if (!committed) throw new RuntimeException("place commit problem");

		int result = PLACE_OK;
		TPoint[] pieceBody = piece.getBody();
		int xx , yy;

		copyBoard( grid , gridBackUp , widths , widthsBackUp );
		committed = false;
		if( outOfBound(x , y) )
		{
			result = PLACE_OUT_BOUNDS;
			//committed = false;
			return result;
		}


		for( int i = 0 ; i < pieceBody.length ; i++ )
		{
			xx = x + pieceBody[i].x;
			yy = y + pieceBody[i].y;
			if( outOfBound(xx , yy) )
				result = PLACE_OUT_BOUNDS;
			else if( grid[xx][yy] == true )
				result = PLACE_BAD;
			else if( grid[xx][yy] == false )
			{
				//heights[xx] = Math.max( heights[xx] , yy + 1 ); // update heights[]
				updateHeights( xx , yy );
				widths[yy] += 1; 								//update widths[]
				updateMaxHeight( xx );
				updateGrid( xx , yy );
				if( widths[yy] == width && result == PLACE_OK )						//check ROW_FILLED
				{
					result = PLACE_ROW_FILLED;
				}
			}
		}
		return result;
	}
	
	private boolean rowFilled( int y )
	{
		return ( widths[y] == width );
	}
	/**
	 Deletes rows that are filled all the way across, moving
	 things above down. Returns the number of rows cleared.
	*/
	private void gridCopyRow( int src , int des )
	{
		for( int i = 0 ; i < width ; i++ )
		{
			grid[i][des] = grid[i][src];
			grid[i][src] = false;
		}
		widths[des] = widths[src];
		widths[src] = 0;
	}

	private void calHeightsAndMaxHeight()
	{
		maxHeight = 0;
		for( int i = 0 ; i < width ; i++ )
		{
			heights[i] = 0;
			for( int j = height - 1 ; j >= 0 ; j-- )
				if( grid[i][j] == true ) {
					heights[i] = j + 1;
					break;
				}
			maxHeight = Math.max( maxHeight , heights[i] );
		}
	}
	public int clearRows() {
		int rowsCleared = 0;
		int curr = 0;

		if( committed )
			copyBoard( grid , gridBackUp , widths , widthsBackUp );

		for( int i = 0 ; i < maxHeight ; i++ )
		{
			if( !rowFilled(i) && rowsCleared != 0)
			{
				gridCopyRow( i , curr );
				curr++;
			}
			else if( rowFilled(i) )
			{
				if( rowsCleared == 0 )
					curr = i;
				rowsCleared++;
				for ( int col = 0 ; col < width ; col++ )
					grid[col][i] = false;
				widths[i] = 0;
			}
		}
		calHeightsAndMaxHeight();

		sanityCheck();
		return rowsCleared;
	}



	/**
	 Reverts the board to its state before up to one place
	 and one clearRows();
	 If the conditions for undo() are not met, such as
	 calling undo() twice in a row, then the second undo() does nothing.
	 See the overview docs.
	*/
	private void copyBoard( boolean[][] srcGird , boolean[][] desGird , int[] srcWidths , int[] desWidths )
	{
		for( int i = 0 ; i < srcGird.length ; i++ )
			System.arraycopy( srcGird[i] , 0 , desGird[i] , 0 , srcGird[i].length );
		System.arraycopy( srcWidths , 0 , desWidths , 0 , srcWidths.length );
	}
	private void revertBoard()
	{
		copyBoard( gridBackUp , grid , widthsBackUp , widths );
		calHeightsAndMaxHeight();
	}
	public void undo() {
		// YOUR CODE HERE
		if( committed ) return;
		revertBoard();
		commit();
		sanityCheck();
	}
	
	
	/**
	 Puts the board in the committed state.
	*/
	public void commit() {
		committed = true;
	}


	
	/*
	 Renders the board state as a big String, suitable for printing.
	 This is the sort of print-obj-state utility that can help see complex
	 state change over time.
	 (provided debugging utility) 
	 */
	public String toString() {
		StringBuilder buff = new StringBuilder();
		for (int y = height-1; y>=0; y--) {
			buff.append('|');
			for (int x=0; x<width; x++) {
				if (getGrid(x,y)) buff.append('+');
				else buff.append(' ');
			}
			buff.append("|\n");
		}
		for (int x=0; x<width+2; x++) buff.append('-');
		return(buff.toString());
	}
}



package oop.asg04;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Arc2D;

/**
 * Created by Thinh on 11/19/2015.
 */
public class JBrainTetris extends JTetris{
    /**
     * Creates a new JTetris where each tetris square
     * is drawn with the given number of pixels.
     *
     * @param pixels
     */
    private JCheckBox brainMode;
    private Brain.Move bestMove;
    private DefaultBrain defaultBrain;
    private JPanel little;
    private JSlider adversary;
    private JLabel adversaryState;
    JBrainTetris(int pixels) {

        super(pixels);
        bestMove = new Brain.Move();
        brainMode = new JCheckBox("Brain active");
        defaultBrain = new DefaultBrain();
        little = new JPanel();
        adversary = new JSlider( 0 , 100 , 0 );
        adversaryState = new JLabel("ok");
    }

    @Override
    public JComponent createControlPanel() {
        JPanel panel = (JPanel) super.createControlPanel();
        panel.add(new JLabel("Brain : "));
        panel.add( brainMode );

        little.add( new JLabel("Adversary:"));
        adversary.setPreferredSize(new Dimension(100, 15));
        little.add(adversary);

        panel.add(little);

        panel.add(adversaryState);
        return panel;
    }

    public Piece pickWorstPiece()
    {
        double worstScore = 1e9;
        Piece result = null;
        Brain.Move worstMove = new Brain.Move();
        for( int i = 0 ; i < pieces.length ; i++ )
        {
            worstMove = defaultBrain.bestMove(board , pieces[i] , board.getHeight() - 4 , worstMove);
            if( worstMove.score < worstScore )
                result = worstMove.piece;
        }
        if( worstMove == null ) return null;
        return result;
    }
    public Piece pickNextPiece()
    {
        int seed = (int) (Math.random() * 99);
        //System.err.println(seed);
        if( seed >= adversary.getValue() ) {
            adversaryState.setText("ok");
            return super.pickNextPiece();
        }
        else {
            adversaryState.setText("*ok*");
            return pickWorstPiece();
        }
    }

    public void genBestMove()
    {
        bestMove = new Brain.Move();
        defaultBrain.bestMove( board , currentPiece , board.getHeight() - 4, bestMove );
    }

    public void addNewPiece()
    {
        super.addNewPiece();
        board.undo();
        genBestMove();
    }

    public void tick( int verb )
    {
        if( brainMode.isSelected() && verb == DOWN )
        {
            if( bestMove.piece == null )
            {
                stopGame();
                return;
            }
            if( currentPiece != bestMove.piece )
                super.tick(ROTATE);

            if( currentX < bestMove.x )
                super.tick(RIGHT);
            else if( currentX > bestMove.x )
                super.tick(LEFT);
            super.tick(DOWN);
        }
        else
            super.tick(verb);
    }


    public static void main( String args[] )
    {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch (Exception ignored) { }


        JBrainTetris tetris = new JBrainTetris(16);
        JFrame frame = JBrainTetris.createFrame(tetris);
        frame.setVisible(true);
    }

}
